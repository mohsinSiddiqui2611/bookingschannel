<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bookings Channel</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Travelix Project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
    @if( Request::is('/'))
    <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    @elseif(Request::is('about')||Request::is('about/*'))
    <link rel="stylesheet" type="text/css" href="styles/about_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
    @elseif(Request::is('service')||Request::is('service/*'))
        <link rel="stylesheet" type="text/css" href="styles/offers_styles.css">
        <link rel="stylesheet" type="text/css" href="styles/offers_responsive.css">
    @elseif(Request::is('contact-us')||Request::is('contact-us/*')||Request::is('contact')||Request::is('contact/*'))
        <link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
        <link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
        @endif
</head>

<body>

<div class="super_container">
    @include('layouts.header')
@yield('content')
    @include('layouts.footer')
</div>
    @include('layouts.scripts')
</body>

</html>

