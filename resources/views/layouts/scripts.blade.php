
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
@if(!Request::is('/'))
    <script src="plugins/parallax-js-master/parallax.min.js"></script>
@endif
@if(Request::is('/'))
<script src="js/custom.js"></script>
@elseif(Request::is('contact-us')||Request::is('contact-us/*')||Request::is('contact')||Request::is('contact/*'))
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="js/contact_custom.js"></script>
@endif
