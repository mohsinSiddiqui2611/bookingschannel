<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index' );
Route::get('/about-us', 'HomeController@about' );
Route::get('/about', 'HomeController@about' );
Route::get('/service', 'HomeController@services' );
Route::get('/contact', 'HomeController@contact' );
Route::get('/contact-us', 'HomeController@contact' );
