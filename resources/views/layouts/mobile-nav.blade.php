
<div class="menu trans_500">
    <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
        <div class="menu_close_container"><div class="menu_close"></div></div>
        <div class="logo menu_logo"><a href="\"><img src="images/logo.png" alt=""></a></div>
        <ul>
            <li class="menu_item"><a href="\">home</a></li>
            <li class="menu_item"><a href="\about">about us</a></li>
            <li class="menu_item"><a href="\service">offers</a></li>
            <li class="menu_item"><a href="\contact">contact</a></li>
        </ul>
    </div>
</div>